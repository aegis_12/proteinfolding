
#	
#	Monte carlo 
#	

import random
import math
import numpy as np

def writeConfiguration(M, amino, output_path):
	f = open(output_path, 'w')
	f.write("MODEL 1\n")
	f.write("COMMENT: nOfContacts=0\n")

	for i in range(0, len(M)):
		atm = 'LYS' if amino[i] else 'ALA'
		f.write("ATOM      "+str(i)+"  CA  "+atm+"   "+str(i)+"  A     "+str(M[i][0])+"   "+str(M[i][1])+"   0.0\n")
	
	f.write("ENDML\n")
	f.close()

def simulation2pdb(simulation, hp_polymer, output_path):
    
    f = open(output_path, "w")
    aa_type = ["LYS" if x=="P" else "ALA" for x in hp_polymer.tostring() ]
    
    for m in xrange(len(simulation)):
      temp, matrix, number_of_contacts, energy = simulation[m][0], simulation[m][1], simulation[m][2], simulation[m][3]
      f.write("MODEL " + str(m + 1) + "\n")
      f.write("COMMENT: nOfContacts=" + str(number_of_contacts)+"\n")
      
      coords = matrix2coord(matrix)
      for i in xrange(coords.shape[0]):
        c = coords[i].tolist()[0]
        f.write("ATOM      "+str(i)+"  CA  " + aa_type[i] + "   "+str(i)+"  A     " + str(float(c[0])) + "   " + str(float(c[1])) + "   0.000\n")
      f.write("ENDMDL\n")
    f.close()

#	
#	Rotation primitives
#	

def rotate2D(point, origin, movx, movy):
	aux0 = point[0] - origin[0]
	aux1 = point[1] - origin[1]

	xr = movx * aux1
	yr = movy * aux0

	return [xr + origin[0], yr + origin[1]]

def rotate90_counter(point, origin):
	return rotate2D(point, origin, 1, -1)

def rotate90_clock(point, origin):
	return rotate2D(point, origin, -1, 1)

#	
#	Algorithm functions
#	

#	
#	Saber si una configuracion es correcta
#	

def overlap(M):
	aux = ['_'.join(str(x) for x in i) for i in M]
	return len(set(aux)) != len(aux)

#	
#	Para una configuracion, obtener cuales son todos los vecinos
#	lr: left - right, rl: right - left
#	co: counter-clockwise, cl: clockwise
#	

def getAllNeighbors(M):
	movs = []
	for i in range(1, len(M)):
		j = len(M) - i

		lr_cl, lr_co = M[:], M[:]

		lr_co[i:] = [rotate90_counter(ix, lr_co[i-1]) for ix in lr_co[i:]]
		lr_cl[i:] = [rotate90_clock(ix, lr_cl[i-1]) for ix in lr_cl[i:]]

		rl_cl, rl_co = M[:], M[:]

		rl_co[:j] = [rotate90_counter(ix, rl_co[j]) for ix in rl_co[:j]]
		rl_cl[:j] = [rotate90_clock(ix, rl_cl[j]) for ix in rl_cl[:j]]

		yield [lr_cl, lr_co, rl_cl, rl_co]

#
#	Check which ones are valid
#	Y devuelve tambien el numero de movimientos

def getAllValidNeighbors(M):
	movs = []
	#	i has four items (lr_cl, lr_co, rl_cl, rl_co)
	for i in getAllNeighbors(M):
		#	Check if is valid
		for ix in i:
			if not overlap(ix):
				movs.append(ix)
	
	return movs, len(movs)

#
#	Check which ones are connected
#

def getEnergy(M, amino):

	hb_bonds = 0

	for i in range(0, len(M)):
		for j in range(0, len(M)):
			if abs(i - j) > 1:	# 	Evitar el de antes y el de despues pues estan conectados seguro

				#	UP - DOWN
				if 	(M[i][0] == M[j][0] and M[i][1] + 1 == M[j][1]) or \
					(M[i][1] == M[j][1] and M[i][0] + 1 == M[j][0]):
					
					#	Si los dos son uno. a la lista de hb bonds, sino a la normal
					if amino[i] == 1 and amino[j] == 1:
						hb_bonds += 1

	return -hb_bonds

def argmin(lst):
	return lst.index(min(lst))

#	
#	Simulated Annealing
#	http://cgis.cs.umd.edu/class/spring2003/cmsc838t/papers/ungerandmoult1993.pdf
#	http://www.acsu.buffalo.edu/~phygons/cp2/topic3/genetic.py
#

def simulatedAnnealing(M, amino):
	
	#	Initial values
	steps = 50 * 10**6
	T = 2

	#	Energy
	minEnergy = lstEnergy = getEnergy(M, amino)
	minM = M

	done = True

	for step in range(0, steps):

		#	Get new monte carlo value
		if done:
			neighbors, num = getAllValidNeighbors(M)
			done = False

		newM = random.choice(neighbors)

		#	Energy of the new configuration
		newEnergy = getEnergy(M, amino)

		#	Check difference in energy wrt the last one
		dE = newEnergy - lstEnergy
		if random.random() < math.exp(-dE/T):
			
			#	Update the energy
			lstEnergy = newEnergy
			M = newM
			done = True 
			
			#	Check if is min
			if lstEnergy < minEnergy:
				minEnergy = lstEnergy
				minM = M

		#	Update temperature (Like on the paper)
		if step % 200000 == 0 and T > 0.15:
			T *= 0.99

	return minM, minEnergy

#	
#	MCsearch
#	http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-8-342#CR43
#	

def MCsearch(C, amino, T, steps=100):
	
	#	Energia principal
	C_energy = getEnergy(C, amino)

	for i in range(0, steps):
		
		#	Nuevo vecino
		C_prima = C[:]
		C_neighbors, num = getAllValidNeighbors(C_prima)
		C_prima = random.choice(C_neighbors)

		#	Energy
		C_prima_energy = getEnergy(C_prima, amino)
		dE = C_prima_energy - C_energy

		if dE <= 0:
			C = C_prima[:]
			C_energy = C_prima_energy
		else:
			if random.random() < math.exp(-dE/T):
				C = C_prima[:]
				C_energy = C_prima_energy

	return C, C_energy



#	
#	REMCSimulation (usando MCSearch)
#	

def REMCSimulation(C, amino):
	
	numReplicas = 7
	maxSteps = 100

	minEnergy = 0
	offset = 0

	TMax = 1
	TMin = 0.15

	#	Rangos de temperaturas
	T = np.linspace(TMin, TMax, numReplicas).tolist()

	#	Initial set of replicas
	replicas = [C for i in T]
	energy   = [0 for i in T]

	while maxSteps:

		print maxSteps

		#	Actualizar valores de las replicas
		for i in range(0, len(replicas)):
			Aux_replicas, Aux_energy = MCsearch(replicas[i], amino, T[i])
			
			replicas[i] = Aux_replicas
			energy[i] 	= Aux_energy

			#	Crec que esta part es pot llevar no?
			if energy[i] < minEnergy:
				minEnergy = energy[i]

		i = offset + 1
		while i + 1 <= numReplicas:
			j = i + 1
			diff = (1/T[j] - 1/T[i]) * (energy[j] - energy[i])
			
			if diff <= 0:
				#	swap
				T[i], T[j] = T[j], T[i]
			else:
				if random.random() <= math.exp(-diff):
					#	swap
					T[i], T[j] = T[j], T[i]
			i += 2

		maxSteps -= 1

	#	La energia total de cada replica
	#	Devolver la organizacion de mayor energia

	min_energy_indx = argmin(energy)
	return replicas[min_energy_indx], energy

#	
#	Start
#	

def initial():
	return [1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1]

def other():
	return [0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0]

def initialize(amino):
	return [[i, 0] for i in range(0, len(amino))]

#	Start

amino = other()
M = initialize(amino)

#	simulatedAnnealing(M, amino)

newM, energy = REMCSimulation(M, amino)
print newM
print energy

writeConfiguration(newM, amino, 'uno.pdb')
